package ch.tocco;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.jar.Attributes;
import java.util.jar.JarInputStream;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import java.util.zip.ZipEntry;

/**
 * Merges multiple JARs into one
 * - MANIFEST.MF is taken from the first jar and may be amended
 * - if a file exists in multiple jars, the first one is used
 * - if the same services are defined in multiple jars (Same filename in /META-INF/services/) those service definitions are "merged" (every definition after the fist is appended to the first)
 *
 * Usage:
 * - create folders "input/"
 * - place jars in "input/"
 * - amend output filename and manifest entries as required in this class
 */
public class JarMerger {

    public static void main(String[] args) throws IOException {
        //amend input folder & output path / jar name as required
        String inputFolder = "input";
        String outputJarName = "output/lucene-all-6.4.1.jar";

        File jarsFolder = new File("input");
        Path outputFolder = Files.createTempDirectory("merged_jar_content");
        File[] jarFiles = jarsFolder.listFiles((dir, name) -> name.endsWith(".jar"));
        if (jarFiles == null || jarFiles.length == 0) {
            System.out.printf("no jar files found in folder '%s'%n", inputFolder);
            return;
        }

        for(int i = 0; i < jarFiles.length; i++) {
            copyJarContent(jarFiles[i], outputFolder);
        }

        //override Manifest attributes as required
        Manifest manifest = getManifestOfFirstJar(jarFiles);
        manifest.getMainAttributes().put(Attributes.Name.SPECIFICATION_TITLE, "Lucene Search Engine: all");

        File outputJar = new File(outputJarName);
        zipFolder(outputFolder, outputJar.toPath(), manifest);
        System.out.printf("successfully combined %d jars from '%s' into '%s'", jarFiles.length, inputFolder, outputJarName);
    }

    private static Manifest getManifestOfFirstJar(File[] jarFiles) throws IOException {
        try (JarInputStream jis = new JarInputStream(new FileInputStream(jarFiles[0]))) {
            return jis.getManifest();
        }
    }

    private static void copyJarContent(File jarFile, Path target) throws IOException {
        FileSystem fileSystem = FileSystems.newFileSystem(jarFile.toPath(), null);
        Path jarPath = fileSystem.getPath("/");

        Files.walkFileTree(jarPath, new SimpleFileVisitor<Path>() {
            private Path currentTarget;

            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                currentTarget = target.resolve(jarPath.relativize(dir).toString());
                Files.createDirectories(currentTarget);
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Path targetPath = target.resolve(jarPath.relativize(file).toString());
                if (file.getFileName().toString().equals("MANIFEST.MF")) {
                    return FileVisitResult.CONTINUE;
                }

                if (targetPath.toFile().exists()) {
                    if (file.toString().startsWith("/META-INF/services/")) {
                        mergeServiceDefinition(file, targetPath);
                    }
                } else {
                    Files.copy(file, targetPath);
                }
                return FileVisitResult.CONTINUE;
            }

            private void mergeServiceDefinition(Path file, Path targetPath) throws IOException {
                try (BufferedReader reader = Files.newBufferedReader(file);
                     PrintWriter writer = new PrintWriter(Files.newBufferedWriter(targetPath, StandardOpenOption.APPEND))) {
                    reader.lines()
                        .filter(l -> !l.isEmpty())
                        .filter(l -> !l.startsWith("#"))
                        .forEach(writer::println);
                }
            }
        });
    }

    private static void zipFolder(Path sourceFolderPath, Path zipPath, Manifest manifest) throws IOException {
        JarOutputStream jos = new JarOutputStream(new FileOutputStream(zipPath.toFile()), manifest);
        Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                jos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
                Files.copy(file, jos);
                jos.closeEntry();
                return FileVisitResult.CONTINUE;
            }
        });
        jos.close();
    }
}
